import pickle
import sys
import timeit
import numpy as np
np_load_old = np.load
# modify the default parameters of np.load
np.load = lambda *a,**k: np_load_old(*a, allow_pickle=True, **k)
from sklearn.metrics import roc_auc_score, precision_score, recall_score
from sklearn.utils import shuffle
import tensorflow as tf 
from tensorflow.keras.layers import Dense, Flatten, Conv2D, Embedding 
from tensorflow.keras import Model


def input_data():
    compounds = np.load(dir_input + 'compounds.npy')
    adjacencies = np.load(dir_input + 'adjacencies.npy')
    proteins = np.load(dir_input + 'proteins.npy')
    interactions = np.load(dir_input + 'interactions.npy')
    # fingerprint_dict = load_pickle(dir_input + 'fingerprint_dict.pickle')
    # word_dict = load_pickle(dir_input + 'word_dict.pickle')
    # n_fingerprint = len(fingerprint_dict)
    # n_word = len(word_dict)
    # print(compounds.shape, adjacencies.shape, proteins.shape, interactions.shape)
    # print(compounds)

    # compounds, adjacencies, proteins, interactions = shuffle(compounds, adjacencies, proteins, interactions)
    print(compounds.shape, adjacencies.shape, proteins.shape, interactions.shape)
    print(len(compounds[:3]))
    l_data = len(compounds)
    def data_generator():
        for com, ad, pr, it in zip(compounds, adjacencies, proteins, interactions):
            if it==0:
                binary_results = [1.0, 0.0]
            else:
                binary_results = [0.0, 1.0]
            yield {"input_1":com, "input_2":ad, "input_3":pr}, binary_results
    output_types = ({"input_1": tf.int64, "input_2": tf.int64, "input_3" : tf.int64}, tf.int64)
    dataset = tf.data.Dataset.from_generator(data_generator, output_types)

    # dataset = dataset.batch(32)
    # return compounds, adjacencies, proteins, interactions
    return dataset

def load_pickle(file_name):
    with open(file_name, 'rb') as f:
        return pickle.load(f)

def shuffle_dataset(dataset, seed):
    np.random.seed(seed)
    np.random.shuffle(dataset)
    return dataset

def split_dataset(dataset, ratio):
    n = int(ratio * len(dataset))
    dataset_1, dataset_2 = dataset[:n], dataset[n:]
    return dataset_1, dataset_2

class My_bi_attetion(Model):
    """docstring for My_bi_attetion"""
    def __init__(self):
        super(My_bi_attetion, self).__init__()
        self.embed_fingerprint = Embedding(n_fingerprint, dim, mask_zero=True)
        self.embed_word = Embedding(n_word, dim, mask_zero=True)

    def gnn(self, fi, ad, n_layers):
        for _ in range(n_layers):
            # print(fi.shape)
            non_linear = Dense(dim, activation='relu')(fi)
            # print(fi.shape)
            # print(ad.shape)
            fi = fi + tf.matmul(ad, non_linear)
        return fi

    def protein_cnn(self, pr, n_layers):
        for _ in range(n_layers):
            # print(pr.shape)
            pr = Conv2D(1, (window*2+1, 10), padding="same")(pr)
        return pr

    def cross_attention(self, queries, values, query_mask=None, value_mask=None):
        # if query_mask!=None and value_mask!=None:
        #     attended_vec = tf.keras.layers.AdditiveAttention()(inputs=[queries, values], mask=[query_mask, value_mask])
        # else :        
        #     attended_vec = tf.keras.layers.AdditiveAttention()([queries, values])
        attended_vec = tf.keras.layers.AdditiveAttention()(inputs=[queries, values], mask=[query_mask, value_mask])
        return attended_vec

    def self_add_attention(self, values, self_mask):
        query = tf.ones([1, 1])
        # mask_query = tf.keras.layers.Masking(mask_value=0.)
        # print(mask_query)
        self_attend = tf.keras.layers.AdditiveAttention()(inputs=[query, values], mask=[None, self_mask])
        # pass
        return self_attend

    def call(self, x):
        fig = x['input_1']
        adj = x['input_2']
        adj = tf.dtypes.cast(adj, tf.float32)
        pro = x['input_3']
        # print(pro)

        node_vec = self.embed_fingerprint(fig)
        mask_node = node_vec._keras_mask
        pro_vec = self.embed_word(pro)
        mask_pro = pro_vec._keras_mask

        pro_vec = pro_vec[..., tf.newaxis]

        message_passed = self.gnn(node_vec, adj, layers_gnn)

        protein_conv = self.protein_cnn(pro_vec, layers_cnn)
        protein_conv = tf.squeeze(protein_conv, -1)

        pro_attended = self.cross_attention(message_passed, protein_conv, query_mask=mask_node, value_mask=mask_pro)
        mol_attended = self.cross_attention(protein_conv, message_passed, query_mask=mask_pro, value_mask=mask_node)


        vec_pro_attended = self.self_add_attention(pro_attended, mask_node)
        vec_mol_attended = self.self_add_attention(mol_attended, mask_pro)

        last_concat = tf.concat([vec_mol_attended, vec_pro_attended], -1)

        mlp1 = Dense(20, activation='relu')(last_concat)
        mlp2 = Dense(5, activation='relu')(mlp1)
        output = Dense(2, activation='softmax')(mlp2)
        # output = tf.squeeze(output, -1)
        return output
    
if __name__ == "__main__":
    DATASET='human'
    # DATASET=celegans
    # DATASET=yourdata
    radius='2'
    ngram='3'
    dir_input = ('../dataset/' + DATASET + '/input/'
                     'radius' + radius + '_ngram' + ngram + '/')
    # take len dict of fingerprint and protein
    fingerprint_dict = load_pickle(dir_input + 'fingerprint_dict.pickle')
    word_dict = load_pickle(dir_input + 'word_dict.pickle')
    n_fingerprint = len(fingerprint_dict)
    n_word = len(word_dict)
    dim = 10
    layers_gnn = 3
    side=5
    window = 2*side + 1
    layers_cnn = 3
    batch_size = 8
    epoch_num = 5

    data = input_data()
    # batched_data = data.batch(1)
    batched_data = data.padded_batch(batch_size, padded_shapes=({'input_1' : [None],
                                                'input_2' : [None, None],
                                                'input_3' : [None]}, 
                                                [None]))
    batched_data.shuffle(10000)
    model = My_bi_attetion()
    bce = tf.keras.losses.BinaryCrossentropy()
    # bce = tf.keras.losses.MeanAbsoluteError()
    optimizer = tf.keras.optimizers.Adam(learning_rate=2e-4, amsgrad=False)

    train_loss = tf.keras.metrics.Mean(name='train_loss')
    train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='train_accuracy')

    # @tf.function
    def Trainer(inputs, labels):
        with tf.GradientTape() as tape:
            outputs = model(inputs)
            loss = bce(labels, outputs)
            print(labels, outputs)
        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

        train_loss(loss)
        train_accuracy(labels, outputs)
        return loss

    for x in batched_data:
        inputs, labels = x
        # labels = tf.dtypes.cast(labels, tf.float32)
        # print(labels.shape)
        # labels = tf.one_hot([0.0,1.0], 2)
        # print(labels)
        loss = Trainer(inputs, labels)
        print(loss.numpy())

    # for epoch in range(epoch_num):
    #     for x in batched_data.take(100):
    #         inputs, labels = x
    #         labels = tf.dtypes.cast(labels, tf.float32)
    #         Trainer(inputs, labels)
            
    #         template = 'Epoch {}, Loss: {}, Accuracy: {}'
    #         print(template.format(epoch+1,
    #                     train_loss.result(),
    #                     train_accuracy.result()*100,
    #                     ))

    #         train_loss.reset_states()
    #         train_accuracy.reset_states()







            # print(x)
            # model = My_bi_attetion()
            # out = model(a)
            # bce = tf.keras.losses.BinaryCrossentropy()
            # print(out.shape)
            # print(b.shape)

            # loss = bce(out, b)
            # print(loss.numpy())

            # optimizer = tf.keras.optimizer.Adam(learning_rate=1e-3, amsgrad=True)

